import Foundation

protocol MilkShake{
    func getTaste() -> String
}

class ConcreteMilkShake : MilkShake {
    func getTaste() -> String{
    return "Is milk"
    }
}

class MilkShakeDecorator : MilkShake{
    let addTaste: MilkShake

    required init(addTaste:MilkShake) {
        self.addTaste = addTaste
    }

    func getTaste() -> String {
        return addTaste.getTaste()
    }
}

class Chocolate:MilkShakeDecorator {
    required init(addTaste : MilkShake) {
        super.init(addTaste: addTaste)
       print("Adding chocolate to the milkshake")
    }
    override func getTaste() -> String {
        return "Chocolate milkshake"
    }
}

class Banana:MilkShakeDecorator {
    required init(addTaste : MilkShake) {
        super.init(addTaste: addTaste)
       print("Adding banana to the milkshake")
    }
    override func getTaste() -> String {
        return "Banana milkshake"
    }
}

var  shake : MilkShake = ConcreteMilkShake()
print(shake.getTaste())

shake = Chocolate (addTaste:shake)
print(shake.getTaste())

shake = Banana(addTaste:shake)
print(shake.getTaste())




