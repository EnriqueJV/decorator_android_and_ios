
interface Milk{
    fun getTaste()
}

class ConcreteMilkshake () : Milk{
    override fun getTaste() {
        print ("Milk")
    }
}
open class MilkShakeDecorator (var milkshake: Milk ):Milk{
    override fun getTaste() {
        this.milkshake.getTaste()
    }
}

class ChocolateShake(milkshake : Milk): MilkShakeDecorator(milkshake) {
    override fun getTaste(){
        super.getTaste()
        this.addTaste();
        println("Chocolate shake")
    }
    public fun addTaste(){
        println("Adding chocolate to the milk shake")
    }
}

class BananaShake(milkshake : Milk): MilkShakeDecorator(milkshake) {
    override fun getTaste(){
        super.getTaste()
        this.addTaste();
        println("Banana shake")
    }
    public fun addTaste(){
        println("Adding banana to the milk shake")
    }
}

fun main(args: Array<String>) {
    val chocolateMilkShake = ChocolateShake(ConcreteMilkshake())
    chocolateMilkShake.getTaste()
    var bananaMilkShake = BananaShake(ConcreteMilkshake())
    bananaMilkShake.getTaste()
}